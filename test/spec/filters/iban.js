'use strict';

describe('Filter: iban', function () {

  // load the filter's module
  beforeEach(module('sorokinFrontendApp'));

  // initialize a new instance of the filter before each test
  var iban;
  beforeEach(inject(function ($filter) {
    iban = $filter('iban');
  }));

  it('should return the input prefixed with "iban filter:"', function () {
    var text = 'angularjs';
    expect(iban(text)).toBe('iban filter: ' + text);
  });

});
