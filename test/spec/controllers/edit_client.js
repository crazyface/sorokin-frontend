'use strict';

describe('Controller: EditClientCtrl', function () {

  // load the controller's module
  beforeEach(module('sorokinFrontendApp'));

  var EditClientCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    EditClientCtrl = $controller('EditClientCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EditClientCtrl.awesomeThings.length).toBe(3);
  });
});
