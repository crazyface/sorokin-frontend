'use strict';

describe('Controller: InnerCtrl', function () {

  // load the controller's module
  beforeEach(module('sorokinFrontendApp'));

  var InnerCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    InnerCtrl = $controller('InnerCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(InnerCtrl.awesomeThings.length).toBe(3);
  });
});
