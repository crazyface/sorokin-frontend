'use strict';

/**
 * @ngdoc service
 * @name sorokinFrontendApp.client
 * @description
 * # client
 * Service in the sorokinFrontendApp.
 */
angular.module('sorokinFrontendApp')
  .service('Client', function (Restangular) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var client = Restangular.service('client');
    return client
  });
