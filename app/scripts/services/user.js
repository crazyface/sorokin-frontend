'use strict';

/**
 * @ngdoc service
 * @name sorokinFrontendApp.user
 * @description
 * # user
 * Service in the sorokinFrontendApp.
 */
angular.module('sorokinFrontendApp')
  .service('User', function (Restangular) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var user = Restangular.service('user');
    return user
  });
