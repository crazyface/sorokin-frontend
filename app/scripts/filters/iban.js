'use strict';

/**
 * @ngdoc filter
 * @name sorokinFrontendApp.filter:iban
 * @function
 * @description
 * # iban
 * Filter in the sorokinFrontendApp.
 */
angular.module('sorokinFrontendApp')
  .filter('iban', function () {
    return function (input) {
      return input.replace(/(.{4})/g, '$1 ').trim();
    };
  });
