'use strict';

/**
 * @ngdoc function
 * @name sorokinFrontendApp.controller:EditClientCtrl
 * @description
 * # EditClientCtrl
 * Controller of the sorokinFrontendApp
 */
angular.module('sorokinFrontendApp')
  .controller('EditClientCtrl', function ($scope, $rootScope, Restangular, data,
                                          $uibModalInstance, FileUploader, API_ROOT,
                                          $auth) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.uploader = new FileUploader({
      autoUpload: true,
      headers: {
        Authorization: 'JWT ' + $auth.getToken()
      },
      url: API_ROOT + 'client/upload/',
      onSuccessItem: function (item, response, status, headers) {
        $scope.instance.avatar = response.path;
        $scope.instance.avatar_url = response.url;
      },
      onErrorItem: function (item, response, status, headers) {
        if($scope.forms.main.backendErrors)
          $scope.forms.main.backendErrors['avatar'] = [response.error]
        else
          $scope.forms.main.backendErrors = {'avatar': [response.error]};
      }
    });

    $scope.clearPhoto = function(intance){
      intance.avatar = null;
      intance.avatar_url = null;
    };

    $scope.forms = {};
    var action;
    if(data.instance.id){
      action = 'update';
      $scope.instance = data.instance.clone();
    }else{
      action = 'create';
      $scope.instance = Restangular.restangularizeElement(null, angular.extend({}, data.instance), 'client');
    }

    $scope.save = function(instance){
      if($scope.forms.main.$valid){
        instance.save().then(function(response){
          $uibModalInstance.close({action: action, instance: response});
        }).catch(function(response){
          if(response.status == 400)
            $scope.forms.main.backendErrors = response.data;
          else if (response.status == 403)
            $rootScope.login()
        });
      }
    };
    $scope.cancel = function(){
      $uibModalInstance.dismiss();
    }

    $scope.delete = function(instance){
      if(confirm('Are you sure?')){
        instance.remove().then(function(){
          $uibModalInstance.close({action: 'delete'});
        })
      }
    }
  });
