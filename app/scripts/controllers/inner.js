'use strict';

/**
 * @ngdoc function
 * @name sorokinFrontendApp.controller:InnerCtrl
 * @description
 * # InnerCtrl
 * Controller of the sorokinFrontendApp
 */
angular.module('sorokinFrontendApp')
  .controller('InnerCtrl', function ($scope, $rootScope, $auth, User) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $rootScope.$auth = $auth;

    $rootScope.login = function () {
      $auth.authenticate('google', {provider: 'google-oauth2'}).then(function(response){
        $scope.user = User.one(response.data.id).get().$object;
      });
    }
    $rootScope.logout = function () {
      if(confirm('Are you sure you want to logout?')){
        $scope.user = null;
        $auth.logout();
      }
    }
  });
