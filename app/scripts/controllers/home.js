'use strict';

/**
 * @ngdoc function
 * @name sorokinFrontendApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the sorokinFrontendApp
 */
angular.module('sorokinFrontendApp')
  .controller('HomeCtrl', function ($scope, Client, $uibModal) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.clients = Client.getList().$object;
    $scope.editClient = function(client){
      var modalInstance = $uibModal.open({
          templateUrl: 'views/edit_client.html',
          controller: 'EditClientCtrl',
          resolve: {
            data: function () {
              return {
                instance: client,
              }
            }
          }
        });
        modalInstance.result.then(function (data) {
          if(data.action == 'update'){
            angular.extend(client, data.instance);
          }
          else if (data.action == 'create'){
            $scope.clients.push(data.instance);
          }else if (data.action == 'delete'){
            var index = $scope.clients.indexOf(client);
            $scope.clients.splice(index, 1);
          }

        });
    }
  });
