'use strict';

/**
 * @ngdoc function
 * @name sorokinFrontendApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the sorokinFrontendApp
 */
angular.module('sorokinFrontendApp')
  .controller('LoginCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
