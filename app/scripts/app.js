'use strict';

/**
 * @ngdoc overview
 * @name sorokinFrontendApp
 * @description
 * # sorokinFrontendApp
 *
 * Main module of the application.
 */
angular
  .module('sorokinFrontendApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'satellizer',
    'route-segment',
    'view-segment',
    'ui.bootstrap',
    'restangular',
    'angularFileUpload',
  ])
  .constant("API_ROOT", 'http://127.0.0.1:8000/api/')
  .config(function ($locationProvider, $routeProvider, $authProvider,
                    $routeSegmentProvider, API_ROOT, RestangularProvider) {
    $locationProvider.html5Mode(true);


    RestangularProvider.setBaseUrl(API_ROOT);
    RestangularProvider.setRequestSuffix('/');

    $authProvider.google({
      clientId: '210142401761-d02n9g83iftk2kar86s17cf8md3mt1kf.apps.googleusercontent.com',
      url: API_ROOT + 'login/social/jwt_user/',
      scope: ['email'],
      redirectUri: window.location.origin + '/', // backend append trailing slash
    });

    $authProvider.authToken = 'JWT';

    $routeSegmentProvider.
      when('/', 'inner.home').
      segment('inner', {
        templateUrl: 'views/inner.html'
      }).
      within().
      segment('home', {
        templateUrl: 'views/home.html'
      });
  });
