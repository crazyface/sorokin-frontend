'use strict';

/**
 * @ngdoc directive
 * @name sorokinFrontendApp.directive:backendErrors
 * @description
 * # backendErrors
 */
angular.module('sorokinFrontendApp')
  .directive('backendErrors', function () {
    return {
      restrict: 'A',
      scope: {
        form: '=backendErrors',
      },
      link: function postLink(scope, element, attrs) {
        scope.$watch('form.backendErrors', function(newVal){
          if(newVal){
            for(var field in newVal){
              if(field != 'non_field_errors'){
                scope.form[field].$setValidity('backend', false);
              }
            }
          }
        }, true);
        element.on('change paste', 'input, textarea', function(){
          var target = $(this);
          var name = target.attr('name');
          if(scope.form[name])
            scope.form[name].$setValidity('backend', true);
        });
      }
    };
  });
